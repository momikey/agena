# Description

Agena is a simple and not very bright server for the [Gemini](https://gemini.circumlunar.space/) internet protocol.

## Name

The Agena was an unmanned vehicle used by the Gemini missions as a rendezvous target. As a server is automated, while clients are "manned", the name seemed appropriate to this space fanatic.

# Requirements

Agena requires Python 3.7 and a system version of OpenSSL that supports TLS v1.2 at a minimum. This should include all current operating systems.

The server itself has no other dependencies outside the Python standard library, and I will try to hold to this as much as possible. Pytest is only needed if you want to build the tests.

# Installation

Currently, the easiest way to install Agena is by grabbing the source.

```bash
$ git clone https://gitlab.com/momikey/agena
$ cd agena
$ python setup.py install
```

You may also want to install Agena into a virtual environment. In that case, simply create that environment using `venv`, activate it, and install Agena into that. For example, to install into `/opt/agena`:

```bash
# Create the directory and grab the source
$ mkdir /opt/agena
$ cd /opt/agena
$ git clone https://gitlab.com/momikey/agena .

# Create a virtual environment and activate
$ python3 -m venv /opt/agena/venv
$ source /opt/agena/venv/bin/activate

# Install Agena
(venv)$ python setup.py install
```

# Configuration

By default, Agena will read its configuration from a file named `config.json` in the directory from which it is started. Using the `-c` argument on the command line allows you to select a different file. (The file `config-example.json` in this repo, as its name suggests, is an example you can use to get started.)

For a single static host, you only need to specify an object with the following properties, all of which you can also give as CLI arguments (see below):

* `host`: The server's hostname. Agena doesn't do any proxying, so it will check incoming requests against this string. Note that `host` can be a domain name, subdomain, or even an IP address, but these are not interchangeable. For example, `127.0.0.1` and `localhost` are not the same host, as far as Agena is concerned.

* `port` (optional): The port. By default, Gemini uses port 1965, so you probably don't need to change this or, for that matter, even specify it. Normal rules regarding permissions apply, so you may need to run as a privileged user if you've set this to certain values, usually 0-1023.

* `staticroot`: The path from which static assets will be served. You can specify an absolute or relative path; either will be handled appropriately. Any file whose MIME type can be discovered will be served, and Agena has some very basic protection to prevent directory traversal attacks.

* `dynamicroot` (optional): A path to a directory containing Python modules. Each of these will be imported and scanned for dynamic, server-side functions. Examples are given in the `serve/dynamic` directory of the source repo, and a more detailed description is below.

* `certfile` and `keyfile`: Paths to a SSL certificate and private key file valid for the hostname. Self-signed certs are fine (Gemini allows and even encourages them), but some kind of certificate is required.

* `password` (optional): The private key password for your certificate, if needed. If it is needed but not given, you'll be asked for it when you start the server. That's the more secure option, obviously. If you *do* put a password into your configuration, don't check that into source control.

## Virtual hosts

This feature is currently **experimental**.

Agena has some configuration options for virtual hosts ("vhosts"). The configuration file for your server can include a property called `vhost`, which must contain an array of objects. Each object will be of the same format as the "master" host described above, except that the `port` option is not allowed.

(If you're using a wildcard certificate, you shouldn't need to provide separate `certfile`, etc., properties for subdomains, but I haven't tested this yet.)

Example:

```json
{
    "host": "example.com",
    "certfile": "cert.pem",
    "keyfile": "key.pem",
    "staticroot": "serve/example.com/",

    "vhost": [
      {
        "host": "foo.example.com",
        "staticroot": "serve/foo.example.com/"
      },
      {
        "host": "bar.example.com",
        "staticroot": "serve/bar.example.com/",
        "certfile": "some-other-cert.pem",
        "keyfile": "some-other-key.pem"
      }
    ]
}
```

## Server-side scripting

This feature is currently **experimental**.

Agena allows you to write server-side scripts that work similarly to controller functions or methods in web frameworks such as Flask or Express. By specifying the `dynamicroot` option for the master or any virtual host, you can enable both server scripts and dynamic routing. The path you choose will be scanned for Python modules, each of which will be imported and checked.

To write server scripts, you'll need to do the following:

1. Import the `Router` class from the `agena.router` module.
2. Define your own custom subclass, e.g., `MyRouter`. (You can name it whatever you wish. Agena will find it regardless.)
3. Create an instance of this subclass, for example `my_router = MyRouter()`.
4. Use the `Router.register` decorator on your controller functions, passing it the path you wish to match: `@my_router.register('foo')`.

Controller functions take as a positional parameter the router object, and they will be passed a number of keyword arguments:

* "path": The full path that was requested, excluding query parameters.
* "query": A query string, if one was present in the request; None otherwise.
* "request": An object representing the raw request, including client info.

Additionally, if you use a placeholder (`:name`) in the route string, the controller will be passed a keyword argument with that name whose value is the matched portion of the path.

A controller function must return a tuple of length 2 or 3:

1. A Gemini status code. (The `agena.response.StatusCode` enum defines constants for each possible code.)
2. A "meta" field whose content depends on the status; for status 20 (success), it will be the MIME type of the response.
3. An optional body containing the data to send to the client.

### Router hooks

Your router subclass can override the following four methods to be notified of various server events:

* "Router.start(server)": Called after the server has completed its startup and is ready to begin handling requests.
* "Router.before_request(request)": Called before each request, *whether or not it is handled by the router*.
* "Router.after_request(request, response)": Called after each request, *whether or not it is handled by the router*, and it will be passed both the request and the response. (Responses for static documents are handled internally by Agena.)
* "Router.shutdown(server)": Called immediately before the server begins shutting down.

# Usage

```
usage: agena [-h] [-c CONFIG] [--host HOST] [--port PORT] [--certfile CERTFILE] [--keyfile KEYFILE]
                   [--pass PASSWORD] [--static-root STATICROOT]

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        Specify a configuration file (default is "config.json")
  --host HOST           The hostname for this server
  --port PORT           The port for this server
  --certfile CERTFILE   Path to the SSL certificate file
  --keyfile KEYFILE     Path to the SSL private key file
  --pass PASSWORD, --password PASSWORD
                        The password/passphrase for the SSL certificate, if needed
  --static-root STATICROOT
                        The path from which static assets will be served
  --dynamic-root DYNAMICROOT
                        The path to search for dynamic content (i.e., server-side scripts)
```

To start the server:

```bash
$ agena -c <config>
```

If you installed into a virtual environment, you'll need to activate that first (unless it's already activated from a previous step).

```bash
$ source /opt/agena/venv/bin/activate
(venv)$ agena -c <config>
```

Agena does *not* require root or administrator privileges by default. You will only need to run the server with elevated permissions (e.g., via `sudo`) if you want to use a port in the range 0-1023. This is nonstandard, as Gemini defaults to the user-accessible 1965, but the option is there.

At the moment, shutting down is nothing more than using Ctrl-C to interrupt the server process. I'm working on something more sophisticated.

# Roadmap

* Better logging and error handling, of course
* Maybe add IPv6 support (automatic dual-stack requires Python 3.8)
* Whatever else I can think of

# License

Agena is open source software under the [MIT license](./LICENSE).