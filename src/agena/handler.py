# The Agena request handler.

# The handler processes a request and returns an appropriate response
# based on the Gemini specifications.
# Currently, this has several limitations. One, it's static-only; dynamic
# routing is on the roadmap. Two, it has no support yet for query strings
# or user input. Last, it probably needs lots more error handling.

import mimetypes
import logging

from socketserver import StreamRequestHandler
from urllib.parse import urlparse, unquote
from pathlib import Path

from .response import Response, StatusCode, MetaTooLongError
from .router import NoRouteFound

class AgenaHandler(StreamRequestHandler):
    def handle(self):
        self.data = str(self.rfile.readline(), encoding='utf-8')
        logging.debug('Incoming request "%s" from %s', self.data.rstrip('\r\n'), self.request.getpeername()[0])

        # A Gemini request must be terminated with a CRLF pair.
        if not self.data.endswith('\r\n'):
            logging.info('Unterminated request: %s', self.data)
            return self.write_response((StatusCode.BAD_REQUEST, "Unterminated request", None))

        self.data = self.data.strip()

        # Parse the URI.
        url = urlparse(self.data)

        if url.scheme != 'gemini':
            # This is a non-Gemini url, so we don't serve it.
            logging.info("Request has incorrect scheme %s", url.scheme)
            return self.write_response((StatusCode.BAD_REQUEST,
                "Incorrect URI scheme {}".format(url.scheme), None))

        if url.netloc in self.server.vhosts:
            # Search for the requested domain in our list of hosts. If it's there,
            # dispatch to a secondary handler, where we encapsulate all the logic.

            self.host = self.server.vhosts[url.netloc]
            self.routers = self.server.routers.get(url.netloc, [])

            return self.handle_host_request(self.host, url)
        else:
            # The client attempted to request a domain we don't serve.
            # Agena isn't a proxy server. XD
            logging.info("Request for unavailable domain %s", url.netloc)
            return self.write_response((StatusCode.PROXY_REQUEST_REFUSED,
                "Unable to serve request for domain {}".format(url.netloc), None))

    def handle_host_request(self, host, url):
        """Attempt to handle an incoming request to a given hostname."""

        # Now we can try to load the requested resource. This can go bad in
        # many different ways. If it doesn't, then we'll end up with something
        # to send the client.
        
        # URL paths start with the slash unless we're requesting the index.
        if not url.path or url.path == '/':
            document_path = Path(host.static)
        else:
            document_path = Path(host.static) / unquote(url.path[1:])

        # Prevent directory traversal attacks.
        try:
            document_path.resolve().relative_to(Path(host.static).resolve())
        except ValueError:
            # We'll log the malicious attempt, but we don't report it back to the user.
            logging.info("Attempted directory traversal %s", url.path[1:])
            return self.write_response((StatusCode.BAD_REQUEST,
                "Unable to process this request", None))

        # Handle file not existing.
        if not document_path.exists():
            if self.host.dynamic is None or self.routers is None:
                logging.debug("File not found: %s", str(document_path))
                return self.write_response((StatusCode.NOT_FOUND, "File not found", None))
            else:
                logging.debug("Attempting dynamic routing for path %s", url.path)
                for router in self.routers:
                    try:
                        return self.write_response(router.resolve(url.path, self.request,
                            url.query if url.query != '' else None))
                    except NoRouteFound:
                        continue
                else:
                    logging.debug("No route found for path %s", url.path)
                    return self.write_response((StatusCode.NOT_FOUND, "No resource found", None))

        else:
            # If a directory is requested, we search it for a file named 'index.gmi',
            # then return that if it exists. If it doesn't, we instead generate a
            # directory listing page and return that.

            for router in self.routers:
                router.before_request(self.request)

            if document_path.is_dir():
                index = document_path / 'index.gmi'

                if index.exists():
                    logging.debug("Index file found at %s", index)
                    res = self.response_from_file(index)

                else:
                    logging.debug("No index file found at %s, creating directory listing", index)
                    res = (
                        StatusCode.SUCCESS,
                        'text/gemini',
                        self.format_directory_listing(document_path)
                    )

                for router in self.routers:
                    router.after_request(self.request, res)

                return self.write_response(res)

            elif document_path.is_file():
                # For an existing file, we just load and return it.
                res = self.response_from_file(document_path)
                for router in self.routers:
                    router.after_request(self.request, res)

                return self.write_response(res)                

    def write_response(self, res):
        """Write a response object to the socket."""

        status, meta, body = res

        try:
            response = Response(status=status, meta=meta, body=body)

            if response.body:
                logging.info("%d %s (body of %d bytes)",
                    response.status.value, response.meta, len(response.body))
            else:
                logging.info("%d %s", response.status.value, response.meta)

            self.wfile.write(response.format())
            return response.status.value
        except MetaTooLongError as err:
            logging.warning(err.message)
            return Response(StatusCode.PERMANENT_FAILURE, meta=err.message)

    def response_from_file(self, file_path):
        """Get the contents of a requested file and construct a response object."""

        # Python version note: We have to convert the Path object to a string
        # before passing it, because `guess_type()` doesn't accept a path-like
        # object until 3.8.
        mime = mimetypes.guess_type(str(file_path), strict=False)[0]

        logging.debug("Requested file with MIME type %s", mime)

        if mime is not None and mime.startswith('text/'):
            # Text types get encoded to UTF-8.
            with open(file_path, 'r') as outfile:
                body = outfile.read().encode('utf-8')
        else:
            # Anything else is passed to the socket as raw bytes.
            with open(file_path, 'rb') as outfile:
                body = outfile.read()

        return StatusCode.SUCCESS, mime, body

    def format_directory_listing(self, dir_path):
        """Pretty-print a directory listing in Gemini format."""

        root = Path(self.host.static).resolve()
        relative_path = dir_path.resolve().relative_to(root)

        lines = []

        lines.append("# Index of {}".format('/' + str(relative_path)))
        lines.append('')

        # Show a "parent directory" link if we're not at the top of the server.
        if not dir_path.samefile(self.host.static):
            lines.append('=> {} Parent directory'.format(dir_path.resolve().parent.relative_to(root)))

        for f in dir_path.iterdir():
            lines.append('=> {} {}'.format(
                f.resolve().relative_to(root),
                f.relative_to(dir_path)
            ))

        return '\r\n'.join(lines).encode('utf-8')
