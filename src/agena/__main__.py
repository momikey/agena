# The Agena CLI. This allows the user to start an Agena server
# with a variety of options specified on the command line or
# in a JSON configuration file.

import argparse
import logging

from .server import AgenaServer
from .configuration import Configuration

def main():
    # Parse command line options.
    parser = argparse.ArgumentParser(prog="agena")
    parser.add_argument('-c', '--config', help='Specify a configuration file (default is "config.json"', default='config.json')
    parser.add_argument('--host', help="The hostname for this server")
    parser.add_argument('--port', help="The port for this server", type=int)
    parser.add_argument('--certfile', help="Path to the SSL certificate file")
    parser.add_argument('--keyfile', help="Path to the SSL private key file")
    parser.add_argument('--pass', '--password', help="The password/passphrase for the SSL certificate, if needed",
        dest='password')
    parser.add_argument('--static-root', help="The path from which static assets will be served",
        dest='staticroot')
    parser.add_argument('--dynamic-root', help="The path to search for dynamic content (i.e., server-side scripts)",
        dest='dynamicroot')
    parser.add_argument('--log-file', help="Path to a log file; if not provided, logs will be written to STDOUT",
        dest='log_file')
    parser.add_argument('--log-level', help='The log level, one of "debug", "info", "warning", or "error" (default "warn")', 
        default='warning', dest='log_level', choices=['debug', 'info', 'warning', 'error'])
    args = parser.parse_args()

    # Set up logging.
    logging.basicConfig(
        filename=args.log_file,
        level=args.log_level.upper(),
        format='%(asctime)s %(levelname)s %(message)s'
    )

    # Load a configuration file, then merge any arguments from the command line.
    # By setting the "dest" options above, we're able to do a simple loop over
    # the keys for this one, but we have to take out the config file option itself.
    config = Configuration(conf_file=args.config)
    logging.debug("Loaded configuration file %s", args.config)
    args_as_dict = vars(args)
    del args_as_dict['config']

    for arg in args_as_dict:
        if args_as_dict[arg] is not None:
            setattr(config, arg, args_as_dict[arg])

    with AgenaServer(config) as server:
        try:
            logging.info("Started Agena server on port %d", server.port)
            server.serve_forever()
        except KeyboardInterrupt:
            logging.info("Shutting down Agena server")
            server.server_close()

if __name__ == '__main__':
    main()
