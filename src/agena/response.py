### Gemini status and response

from enum import Enum

# Gemini uses two-digit status codes that are a closed set.
class StatusCode(Enum):
    # "Input" status codes prompt the client for an input,
    # which will be suffixed to the same address as a query string.
    # This neither has nor needs an HTTP equivalent.
    # (The only difference between the two codes is that "sensitive"
    # input isn't echoed.)
    INPUT = 10
    SENSITIVE_INPUT = 11

    SUCCESS = 20                # ~= HTTP 200

    TEMPORARY_REDIRECT = 30     # ~= HTTP 302 or 307
    PERMANENT_REDIRECT = 31     # ~= HTTP 301

    TEMPORARY_FAILURE = 40      # ~= HTTP 500
    SERVER_UNAVAILABLE = 41     # ~= HTTP 503
    CGI_ERROR = 42              # no real HTTP equivalent, closest is 500
    PROXY_ERROR = 43            # ~= HTTP 502
    SLOW_DOWN = 44              # ~= HTTP 429

    PERMANENT_FAILURE = 50      # ~= HTTP 400 or 501
    NOT_FOUND = 51              # ~= HTTP 404
    GONE = 52                   # ~= HTTP 410
    PROXY_REQUEST_REFUSED = 53  # no real HTTP equivalent
    BAD_REQUEST = 59            # ~= HTTP 400

    # We probably won't ever need to worry about client certs...
    CLIENT_CERTIFICATE_REQUIRED = 60
    CERTIFICATE_NOT_AUTHORIZED = 61
    CERTIFICATE_NOT_VALID = 62

# The protocol defines the maximum length of a response's META field.
MAX_META_LENGTH = 1024

# Response exception classes

class MetaTooLongError(Exception):
    """The intended meta field is too long."""

    def __init__(self, length):
        self.meta_length = length
        self.message = 'Meta field of {} bytes is too long'.format(length)

# The response object

class Response(object):
    """A Gemini response consists of a header and a body, with the header
    being a status code, a space, any "meta" information, and a CRLF pair.
    """
    def __init__(self, status, meta=None, body=None):
        if isinstance(status, StatusCode):
            self.status = status
        else:
            self.status = StatusCode(status)

        self.meta = meta if meta is not None else ''

        if len(self.meta) > MAX_META_LENGTH:
            raise MetaTooLongError(len(self.meta))

        self.body = body if body is not None else b''

    def format(self):
        """Format the response object as a Gemini response."""
        return '{status} {meta}\r\n'.format(
            status=self.status.value, meta=self.meta).encode('utf-8') + self.body
