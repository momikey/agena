# Agena's dynamic router

from functools import wraps
from string import Template
import urllib.parse
import re

from .response import StatusCode

# Regular expressions for parsing paths/placeholders.
path_match = re.compile(r"(?a:(?P<part>(?:[a-zA-Z0-9_.!~*'()-]|%[a-fA-F0-9]{2})+))")
placeholder_match = re.compile(r":(?P<ph>[a-zA-Z0-9_]+)")

class RouteTemplate(Template):
    """A string template using Express-like placeholder syntax"""
    delimiter = ':'

class NoRouteFound(Exception):
    """No route matched the given path."""

    def __init__(self, path):
        self.path = path

class Router(object):
    """A dynamic router object which enables server-side scripting.

    This is intentionally not tied into the rest of the Agena server,
    because it's intended to be completely optional and opt-in.

    Instead of directly using the router, you are expected to create a
    subclass of it for each Agena virtual host in your environment which
    wants to use server-side scripts. The path to each of these can be
    specified in your server's configuration file, and the handler will
    automatically import them.
    """

    def __init__(self):
        """Create a new router object."""

        self.routes = {}

    def start(self, server):
        """Start this router.

        This method is a hook called by the Agena server after it is created.
        Subclasses may override it for host-specific functionality.

        * "server": The server object.
        """

        pass

    def shutdown(self, server):
        """Stop this router.

        This method is a hook called by the Agena server before it is
        shut down. Subclasses may override it for host-specific functionality.

        * "server": The server object.
        """
        
        pass

    def before_request(self, request):
        """Perform pre-request actions.

        This method is a hook called by the router infrastructure before
        any route-specific controller functions are called. It allows
        higher-level actions to be taken regardless of the specific resource
        requested.

        * "request": The request.
        """

        pass

    def after_request(self, request, response):
        """Perform post-request actions.

        This method is a hook called by the router infrastructure after
        any route-specific controller functions are run.

        * "request": The request.
        * "response": A response from any routing function called, in
            the form of a (status, meta, body) tuple. This is *not* parsed,
            so it may not be a valid Gemini response. If no route was
            called for the given request, "response" will be None.
        """

        pass

    def register(self, route):
        """Register a route for this host. This function should be used
        as a decorator, e.g.:

        @my_router.register('foo')
        def route_foo(path, request):
            ...

        Each route requires a path, and this is specified using Express-like
        syntax, where ":" defines a placeholder.
        
        Given the host "example.com", the router will match the following:

        * Route: "foo"; URI: gemini://example.com/foo
        * Route: "bar/:id"; URI: gemini://example.com/bar/<any string>
        * Route: ":name/home; URI: gemini://example.com/<any name>/home

        Routes are checked in the order in which they are defined.

        Each route function is passed three keyword arguments by the handler:

        * "path": The full path that was requested, excluding query parameters.
        * "query": A query string, if one was present in the request; None otherwise.
        * "request": An object representing the raw request, including client info.

        In addition, a keyword argument corresponding to each placeholder will
        be passed. The name of this argument is that of the placeholder, and
        its value will be the part of the path it matches. For example:

        * Route: "/foo/:id", URI: gemini://example.com/foo/123
            -> path = "foo/123", id = "123"

        (All placeholder arguments are passed as strings.)

        Your route functions/methods must return a tuple of status code,
        "meta" field, and optional body. You may use the StatusCode enum
        defined in the agena.response module for named constants that
        correspond to each possible status code. The contents of the meta
        field depend on the returned status; see the Gemini spec for more
        details.
        """

        def decorator(fn):
            def wrapper(*args, **kwargs):
                result = fn(self, *args, **kwargs)

                # Normalize the result from the routing function into a dict
                # that the handler can use.
                if len(result) == 2:
                    status, meta = result
                    body = None
                elif len(result) == 3:
                    status, meta, body = result
                    if isinstance(body, str):
                        body = body.encode('utf-8')
                else:
                    raise ValueError("Route functions must return a tuple of length 2 or 3")

                return status, meta, body

            # Normalize the path, removing beginning or trailing slashes,
            # then store its function into the routing table.
            self.routes[urllib.parse.unquote(route.strip('/'))] = wrapper
            return wrapper

        return decorator

    def resolve(self, path, request=None, query=None):
        """Attempt to resolve a path using this router."""

        # Normalize the path to match the register function.
        p = urllib.parse.unquote(path.strip('/'))

        for r in self.routes:
            if p == r:
                # Exact match.
                self.before_request(request)
                res = self.routes[r](path=path, request=request, query=query)
                self.after_request(request, res)
                return res
            elif ':' in r:
                # Check for a placeholder match.
                begin, ph, end = placeholder_match.split(r)
                if p.startswith(begin) and p.endswith(end):
                    # This *could* be a match, but we check just in case.
                    if end != '':
                        inner = p[len(begin):-len(end)]
                    else:
                        inner = p[len(begin):]

                    d = { ph: inner }
                    templ = RouteTemplate(r)

                    if templ.substitute(d) == p:
                        # This route matches, so call it.
                        self.before_request(request)
                        res = self.routes[r](path=path, request=request, query=query, **d)
                        self.after_request(request, res)
                        return res

        else:
            self.before_request(request)
            self.after_request(request, None)
            raise NoRouteFound(p)
