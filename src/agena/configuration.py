# An Agena serrver configuration object.

import json
import logging

class Configuration(object):
    """The configuration information for an Agena server. This includes
    a number of important settings, all of which may be read from a JSON
    configuration file:

    * host - (string, required) The hostname of this server.
    * port - (integer) The port number for this server; default is the
        standard Gemini port 1965.
    * staticroot - (string, required) The path from which static assets
        will be served. This may be relative or absolute, and the Agena
        server will prevent any access outside the given directory.

    * certfile - (string) A path to an SSL certificate for this server;
        default is "cert.pem".
    * keyfile - (string) A path to an SSL private key file for this
        server; default is "key.pem".
    * password - (string) The private key password for the SSL file
        indicated in "keyfile". If this option is not present and the key
        file requires a password, you will be prompted when the server
        starts. If you use this option, don't check the configuration file
        into version control, as the password is sensitive information.
    """
    def __init__(self, conf_file=None, conf_string=None, conf_object=None):
        """Read a configuration JSON file and create a config object for it.
        This will load all the known properties as listed in the class
        description, while putting any other key into the `Configuration.meta`
        dictionary.
        
        By using the "conf_string" or "conf_object" arguments, you can instead
        load configuration data from a string or nested dictionary object,
        respectively. The latter is similar to that which would be returned by
        `json.loads()`.
        """

        if conf_file is not None:
            # Handle the base case of a file object.
            with open(conf_file, 'r') as cfile:
                try:
                    data = json.loads(cfile.read(1 << 20))

                except json.JSONDecodeError as err:
                    logging.error("Error reading configuration file at line %d: %s", err.lineno, err.msg)
                    raise err
        elif conf_string is not None:
            # Handle the case where we're given JSON in a string.
            try:
                data = json.loads(conf_string)
            except json.JSONDecodeError as err:
                logging.error("Error reading configuration string: %s", err.msg)
                raise err
        elif conf_object is not None:
            # Or maybe we got an object instead. This might be the case if,
            # for example, some tool already parsed the JSON.
            data = conf_object
        else:
            # If all else fails, don't try to continue. Just tell the user
            # what happened.
            raise ValueError("No valid configuration: you must specify a file, a JSON string, or a dict-like object")

        # Copy data, including defaults where applicable.
        self.host = data['host']
        self.static_root = data['staticroot']
        self.dynamic_root = data.get('dynamicroot')

        self.port = data.get('port', 1965)
        self.cert_file = data.get('certfile', 'cert.pem')
        self.key_file = data.get('keyfile', 'key.pem')
        self.password = data.get('password')

        # Vhosts are their own little thing.
        self.vhosts = []
        for vh in data.get('vhost', []):
            self.vhosts.append(dict(
                host = vh['host'],
                static_root = vh['staticroot'],
                dynamic_root = vh.get('dynamicroot', None),
                cert_file = vh.get('certfile', self.cert_file),
                key_file = vh.get('keyfile', self.key_file),
                password = vh.get('password', None)
            ))

        self.meta = data
