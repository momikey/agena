### The server itself

from socketserver import ThreadingTCPServer, BaseRequestHandler
from collections import namedtuple
from pathlib import Path

import ssl
import mimetypes
import logging
import importlib.util
import inspect

from .handler import AgenaHandler
from .router import Router

GEMINI_DEFAULT_PORT = 1965

VHost = namedtuple('VHost', ['static', 'dynamic'])

class AgenaServer(ThreadingTCPServer):
    """An Agena server listens on the appropriate port, then spawns a new
    thread for any request that comes in."""
    def __init__(self, configuration):
        ThreadingTCPServer.__init__(self,
            ('0.0.0.0',
                getattr(configuration, 'port', GEMINI_DEFAULT_PORT)),
            AgenaHandler)

        # Load up configuration. We don't need to store the SSL stuff, except for
        # a context object that holds all the necessary info.
        self.host = getattr(configuration, 'host', 'localhost')
        self.port = getattr(configuration, 'port', GEMINI_DEFAULT_PORT)

        self.ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        certfile = getattr(configuration, 'cert_file', None)
        keyfile = getattr(configuration, 'key_file', None)
        keypassword = getattr(configuration, 'password', None)

        # If the configuration specifies a certificate and key file (which it should),
        # then we load that certificate. This may require a password, and we consider
        # two possibilities there. One, the config contains it, in which case we simply
        # return that string (in a lambda, because load_cert_chain() expects a function).
        # Alternatively, we do nothing and let OpenSSL pop up a prompt for the user.
        # TODO: I don't know how this interacts with vhosts and multiple certs.
        if certfile is not None and keyfile is not None:
            logging.debug("Loading SSL certificate %s and key file %s", certfile, keyfile)
            self.ssl_context.load_cert_chain(certfile, keyfile,
                (lambda: keypassword) if keypassword is not None else None)

        # Set up routing for the master host.
        self.static_root = getattr(configuration, 'static_root', './')
        if not self.static_root.endswith('/'):
            self.static_root += '/'
        self.dynamic_root = getattr(configuration, 'dynamic_root')
        self.routers = {}

        # Set up virtual hosts, if specified. We'll also add the master host to this list,
        # so that we can use the same API for all of it.
        self.vhosts = { self.host: VHost(self.static_root, self.dynamic_root) }
        for vhost in configuration.vhosts:
            logging.debug("Adding vhost %s", vhost['host'])
            self.vhosts[vhost['host']] = VHost(vhost['static_root'], vhost.get('dynamic_root', None))
            if vhost.cert_file is not None and vhost.key_file is not None:
                self.ssl_context.load_cert_chain(vhost.cert_file, vhost.key_file,
                (lambda: vhost.password) if vhost.password is not None else None)
            
        for vhost in self.vhosts:
            if self.vhosts[vhost].dynamic is not None:
                logging.debug("Loading router configuration for host %s", vhost)
                router_path = Path(self.vhosts[vhost].dynamic)

                if router_path.is_dir():
                    # Load all the modules in this directory.
                    modules = router_path.glob('*.py')
                else:
                    # Treat the path as a single module and load it.
                    modules = [router_path]

                # This is a little hairy. Basically, the idea is to loop over the modules
                # that were specified and extract any objects whose type is a subclass of
                # the Agena Router class. (Direct subclasses will always have the parent
                # class at index 1 of the MRO list. Yes, this is a hack. No, I can't think
                # of another way to scan for *only* these classes. The obvious approach,
                # using issubclass(), will pick up the base class, too.)
                for m in modules:
                    logging.debug("Loading module %s", str(m))
                    spec = importlib.util.spec_from_file_location(m.stem, m)
                    module = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(module)

                    router_classes = [c[1] for c in \
                        inspect.getmembers(module, lambda o: inspect.isclass(o) and o.__mro__[1] == Router)]

                    router_objects = inspect.getmembers(module,
                        lambda o: isinstance(o, tuple(router_classes))
                    )

                    # inspect.gemembers() gives us a (name, class) pair, so extract only
                    # the class itself and store that in our router map.
                    self.routers[vhost] = [r[1] for r in router_objects]

        # Gemini spec mandates TLS 1.2+. My dev PC only has up to 1.2. Oh well.
        # Future incompatibility isn't a problem in the present, right?
        self.ssl_context.minimum_version = ssl.TLSVersion.TLSv1_2

        # Wrap the socket object to use SSL.
        self.socket = self.ssl_context.wrap_socket(self.socket, server_side=True)

        # The "text/gemini" MIME type isn't universally recognized,
        # so we add it to the list of known types.
        mimetypes.init()
        if '.gmi' not in mimetypes.types_map:
            logging.debug("Registering text/gemini MIME type")
            mimetypes.add_type('text/gemini', '.gmi', strict=False)

        # Activate the server and call router "start" callbacks.
        for router_list in self.routers.values():
            for router in router_list:
                router.start(self)

    def server_close(self):
        """Clean up the server and call router "shutdown" callbacks."""

        # Shut down all the routers. If something went wrong in the constructor
        # (such as "Address already in use"), the router map won't get built,
        # so check that, too.
        if getattr(self, 'routers'):
            for router_list in self.routers.values():
                for router in router_list:
                    router.shutdown(self)

        super().server_close()
