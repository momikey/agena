# A lot of this is taken from the PyPA sample setup.py
# (https://github.com/pypa/sampleproject)

from setuptools import setup, find_packages

# Pathlib is 3.4+
import pathlib

top = pathlib.Path(__file__).parent.resolve()

long_description = (top / 'README.md').read_text(encoding='utf-8')

setup(
    name='agena',

    description="A simple Gemini server with minimal dependencies",

    # We loaded this earlier; it's the whole README, basically.
    long_description=long_description,
    # Default is restructuredText, but who uses that anymore?
    long_description_content_type='text/markdown',

    url='https://gitlab.com/momikey/agena',
    author='Michael H. Potter',
    author_email='mhpotter@potterpcs.net',

    # This is always hard, IMO.
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Topic :: Internet',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    keywords='gemini',

    package_dir={'': 'src'},
    packages=find_packages(where='src'),

    python_requires='>=3.7',
    install_requires=[],

    # Dev dependencies.
    extras_require={
        'test': ['pytest']
    },

    project_urls={
        'Issues': 'https://gitlab.com/momikey/agena/issues'
    },

    entry_points={
        'console_scripts': [
            'agena=agena.__main__:main'
        ]
    }
)