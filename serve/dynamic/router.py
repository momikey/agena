from agena.router import Router
from agena.response import StatusCode

from datetime import datetime

# We don't need to customize the router in any way yet.
class ExampleRouter(Router):
    def __init__(self):
        super().__init__()

        self.counter = 0

    def after_request(self, request, response):
        self.counter += 1

my_router = ExampleRouter()

@my_router.register('time')
def route_time(router, **kwargs):
    """Return the current time."""

    time_string = "The current time is {}".format(datetime.now())

    return StatusCode.SUCCESS, 'text/gemini', time_string

@my_router.register('counter')
def route_counter(router, **kwargs):
    """Return the number of requests this server has received since startup."""

    return StatusCode.SUCCESS, 'text/gemini', "Served {} request(s) and counting".format(router.counter)

@my_router.register('echo/:name')
def route_echo(router, **kwargs):
    """Echo a given name."""

    echo_string = "Hello, {}!".format(kwargs['name'])

    return StatusCode.SUCCESS, 'text/gemini', echo_string

@my_router.register('search')
def route_search(router, **kwargs):
    """Search using a query string."""

    all_names = ['Grissom', 'Young', 'McDivitt', 'White', 'Cooper', 'Conrad',
        'Borman', 'Lovell', 'Schirra', 'Stafford', 'Armstrong', 'Scott',
        'Cernan', 'Collins', 'Aldrin']

    query = kwargs.get('query', None)

    if query is None:
        return StatusCode.INPUT, 'Enter a search term'
    else:
        names = [name for name in all_names if query in name.lower()]

        results = ['Search results for {}'.format(query), '']

        for idx, name in enumerate(names):
            results.append('{}. {}'.format(idx+1, name))
            results.append('')

        return StatusCode.SUCCESS, 'text/gemini', '\r\n'.join(results)
