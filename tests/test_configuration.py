import pytest
import json

from agena.configuration import Configuration

mock_config_object = dict(
    host='example.com',
    staticroot='/srv/gemini/'
)

def test_config_file():
    co = Configuration(conf_file='config-example.json')

    assert co.host == 'localhost'
    assert co.port == 1965
    assert co.cert_file == 'cert.pem'
    assert co.password == None
    assert co.static_root == 'serve/static/'

def test_config_object():
    co = Configuration(conf_object=mock_config_object)

    assert co.host == 'example.com'
    assert co.port == 1965
    assert co.cert_file == 'cert.pem'
    assert co.password == None
    assert co.static_root == '/srv/gemini/'

