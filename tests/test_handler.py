import pytest
from agena.handler import AgenaHandler

pytestmark = pytest.mark.parametrize('mock_server', [AgenaHandler], indirect=True)

def test_handler(mock_server):
    send = mock_server.sendall(b'gemini://localhost\r\n')
    assert send is None
    
    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 20
    assert b'text/gemini' in result

def test_bad_request(mock_server):
    # Note: no CR byte, so this is a malformed request.
    send = mock_server.sendall(b'foo\n')
    assert send is None

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 59

def test_bad_scheme(mock_server):
    send = mock_server.sendall(b'https://localhost\r\n')
    assert send is None

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 59

    meta = result[2:].strip()
    assert b"Incorrect URI scheme" in meta

def test_bad_hostname(mock_server):
    send = mock_server.sendall(b'gemini://example.com\r\n')
    assert send is None

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 53

def test_file_not_found(mock_server):
    mock_server.sendall(b'gemini://localhost/foo/\r\n')

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 51

def test_directory_listing(mock_server):
    mock_server.sendall(b'gemini://localhost/example\r\n')

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 20

    meta, body = result[3:].split(b'\r\n', maxsplit=1)
    assert meta == b'text/gemini'
    assert b'Index of' in body

def test_directory_traversal(mock_server):
    mock_server.sendall(b'gemini://localhost/example/../../\r\n')

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 59

def test_good_request(mock_server):
    mock_server.sendall(b'gemini://localhost/example/foo.gmi\r\n')

    result = mock_server.recv(1024)
    status = int(result[0:2])
    assert status == 20

    meta, body = result[3:].split(b'\r\n', maxsplit=1)
    assert meta == b'text/gemini'
    assert body.startswith(b'# Testing')
