import pytest
from agena.router import Router

class MyTestRouter(Router):
    pass

@pytest.fixture
def mock_router():
    my_router = MyTestRouter()

    @my_router.register('foo/')
    def route_foo(router, **kwargs):
        return 51, "Not found"

    @my_router.register('bar/:id')
    def route_bar(router, **kwargs):
        return 51, "Not found {}".format(kwargs['id'])

    @my_router.register('bar/:name/info')
    def route_bar2(router, **kwargs):
        return 51, "Info for user {} not found".format(kwargs['name'])

    return my_router

def test_create_router(mock_router):
    assert mock_router is not None

def test_exact_routing(mock_router):
    res = mock_router.resolve('foo/')

    assert res[0] == 51
    assert res[1] == "Not found"
    assert res[2] is None

def test_placeholder_match(mock_router):
    res = mock_router.resolve('bar/123')

    assert res[0] == 51
    assert res[1].endswith('123')

    res = mock_router.resolve('bar/me/info')

    assert res[0] == 51
    assert not res[1].endswith('123')
