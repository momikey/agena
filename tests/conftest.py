import pytest
import threading
import socket
import mimetypes
from socketserver import ThreadingTCPServer

import agena
from agena.server import VHost

# Fixtures and other test config will go here.

class TestServer(ThreadingTCPServer):
    allow_reuse_address = True

    host, port = ('localhost', 1965)
    static_root = 'serve/static/'
    vhosts = { 'localhost': VHost('serve/static/', None) }
    routers = {}

@pytest.fixture
def add_gemini_mimetype():
    """Add the text/gemini MIME type, if it's not already known."""
    mimetypes.init()
    if '.gmi' not in mimetypes.types_map:
        mimetypes.add_type('text/gemini', '.gmi', strict=False)

@pytest.fixture
def mock_server(request, add_gemini_mimetype):
    """Create a mock server to accept requests. This doesn't use any
    TLS handshake, which means it's not strictly in spec, but we only
    need something to call the Agena request handler."""
    host = ('localhost', 1965)
    server = TestServer(host, request.param)
    server_thread = threading.Thread(target=server.serve_forever)
    client = socket.create_connection(host, 1.0)
    server_thread.start()

    yield client

    client.close()
    server.shutdown()
    server.server_close()
