import pytest

from agena.response import StatusCode, Response, MetaTooLongError

def test_response_input():
    res = Response(StatusCode.INPUT, 'Enter your name:')

    assert res.format() == b'10 Enter your name:\r\n'

def test_response_success():
    res = Response(StatusCode.SUCCESS, 'text/gemini', b'This is a test.')

    assert res.format() == b'20 text/gemini\r\nThis is a test.'

def test_meta_too_long():
    with pytest.raises(MetaTooLongError):
        Response(StatusCode.SUCCESS, 'x' * 1025, b"Body doesn't matter here.")

